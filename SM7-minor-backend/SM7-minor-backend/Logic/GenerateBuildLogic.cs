﻿namespace SM7_minor_backend.Logic;
public class GenerateBuildLogic
{
    public List<AssemblyStep> GenerateBuild(Build pcbuild)
    {
        List<AssemblyStep> buildsteps = new List<AssemblyStep>();
        List<string> armodels = new List<string>();
        List<string> sources = new List<string>();

        armodels.Add("pccase");
        armodels.Add("motherboard");
        armodels.Add("motherboardhighlighted");
        sources.Add("www.test.nl");
        buildsteps.Add(new AssemblyStep(1, "Insert motherboard", "Install motherboard in pc case", "test", "test", "testvid"));

        return buildsteps;
    }
}