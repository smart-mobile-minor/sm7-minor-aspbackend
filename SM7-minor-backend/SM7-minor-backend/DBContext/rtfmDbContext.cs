﻿namespace SM7_minor_backend.DBContext;

using Microsoft.EntityFrameworkCore;
using Models;
public class rtfmDbContext : DbContext
{
    public virtual DbSet<Processor> processors { get; set; }
    public virtual DbSet<Tool> tools { get; set; }
    public virtual DbSet<Case> cases { get; set; }
    public virtual DbSet<Motherboard> motherboards {  get; set; }
    public virtual DbSet<Powersupply> powersupplys {  get; set; }
    public virtual DbSet<AssemblyStep> assemblySteps { get; set; }
    public virtual DbSet<ArModel> arModels { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) => optionsBuilder
            .UseMySQL("server=127.0.0.1;port=3306;user=root;password=;database=rtfmtool");
}

