﻿global using System.Linq.Expressions;
global using SM7_minor_backend.Models;
global using SM7_minor_backend.Controllers;
global using SM7_minor_backend.DBContext;
global using SM7_minor_backend.Repository;
global using SM7_minor_backend.Repository.Interfaces;
global using SM7_minor_backend.Logic;