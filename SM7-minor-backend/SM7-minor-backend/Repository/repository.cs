﻿namespace SM7_minor_backend.Repository;


public class repository<TEntity> : IRepository<TEntity> where TEntity : class
{
    protected readonly rtfmDbContext Context;

    public repository(rtfmDbContext context)
    {
        this.Context = context;
    }

    public void Add(TEntity entity)
    {
        Context.Set<TEntity>().Add(entity).Reload();
        Context.SaveChanges();
    }

    public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
    {
        return Context.Set<TEntity>().Where(predicate);
    }

    public TEntity Get(int id)
    {
        return Context.Set<TEntity>().Find(id);
    }

    public IEnumerable<TEntity> GetAll()
    {
        return Context.Set<TEntity>().ToList();
    }

    public void Remove(TEntity entity)
    {
        Context.Set<TEntity>().Remove(entity);
        Context.SaveChanges();
    }

    public void Update(TEntity entity)
    {
        Context.Set<TEntity>().Update(entity);
        Context.SaveChanges();
    }
}

