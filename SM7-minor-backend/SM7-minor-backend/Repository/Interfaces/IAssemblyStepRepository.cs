﻿namespace SM7_minor_backend.Repository.Interfaces;
public interface IAssemblyStepRepository : IRepository<AssemblyStep>
{
}
