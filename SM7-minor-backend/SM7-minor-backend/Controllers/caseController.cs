﻿namespace SM7_minor_backend.Controllers;

using Microsoft.AspNetCore.Mvc;

[ApiController]
[Route("api/[controller]")]
public class caseController : Controller
{
    private readonly caseRepository _Case;

    public caseController()
    {
        _Case = new caseRepository(new rtfmDbContext());
    }

    [HttpGet("")]
    public async Task<IActionResult> GetAll()
    {
        try
        {
            return Ok(_Case.GetAll());
        }
        catch (Exception)
        {

            return BadRequest();
        }
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> Get(int id)
    {
        try
        {
            return Ok(_Case.Get(id));
        }
        catch (Exception)
        {

            return BadRequest();
        }

    }

    [HttpPost("")]
    public async Task<IActionResult> Post([FromBody] Case entity)
    {
        try
        {
            _Case.Add(entity);

            return Created("www.rtfmtool.com", entity);
        }
        catch (Exception)
        {

            return BadRequest();
        }

    }

    [HttpPut("")]
    public async Task<IActionResult> Put([FromBody] Case entity)
    {
        try
        {
            _Case.Update(entity);

            return Ok(entity);
        }
        catch (Exception)
        {

            return BadRequest();
        }
    }

    [HttpDelete("")]
    public async Task<IActionResult> Delete([FromBody] Case entity)
    {
        try
        {
            _Case.Remove(entity);
            return NoContent();
        }
        catch (Exception)
        {
            return BadRequest();
        }
    }
}