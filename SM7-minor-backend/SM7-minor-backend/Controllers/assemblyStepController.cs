﻿namespace SM7_minor_backend.Controllers;

using Microsoft.AspNetCore.Mvc;

[ApiController]
[Route("api/[controller]")]
public class assemblyStepController : Controller
{
    private readonly assemblyStepRepository _AssemblyStep;
    private readonly GenerateBuildLogic _GenerateBuildLogic;

    public assemblyStepController()
    {
        _AssemblyStep = new assemblyStepRepository(new rtfmDbContext());
        _GenerateBuildLogic = new GenerateBuildLogic();
    }

    [HttpPost("/api/getBuild")]
    public async Task<IActionResult> GetBuildSteps ([FromBody] Build entity)
    {
        try
        {
            
            return Ok(_GenerateBuildLogic.GenerateBuild(entity));
        }
        catch (Exception)
        {

            return BadRequest();
        }
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> Get(int id)
    {
        try
        {
            return Ok(_AssemblyStep.Get(id));
        }
        catch (Exception)
        {

            return BadRequest();
        }

    }

    [HttpPost("")]
    public async Task<IActionResult> Post([FromBody] AssemblyStep entity)
    {
        try
        {
            _AssemblyStep.Add(entity);

            return Created("www.rtfmtool.com", entity);
        }
        catch (Exception)
        {

            return BadRequest();
        }

    }

    [HttpPut("")]
    public async Task<IActionResult> Put([FromBody] AssemblyStep entity)
    {
        try
        {
            _AssemblyStep.Update(entity);

            return Ok(entity);
        }
        catch (Exception)
        {

            return BadRequest();
        }
    }

    [HttpDelete("")]
    public async Task<IActionResult> Delete([FromBody] AssemblyStep entity)
    {
        try
        {
            _AssemblyStep.Remove(entity);
            return NoContent();
        }
        catch (Exception)
        {
            return BadRequest();
        }
    }
}
