﻿namespace SM7_minor_backend.Controllers;

using Microsoft.AspNetCore.Mvc;


[ApiController]
[Route("api/[controller]")]
public class motherboardController : Controller
{
    private readonly motherboardRepository _Motherboard;

    public motherboardController()
    {
        _Motherboard = new motherboardRepository(new rtfmDbContext());
    }

    [HttpGet("")]
    public async Task<IActionResult> GetAll()
    {
        try
        {
            return Ok(_Motherboard.GetAll());
        }
        catch (Exception)
        {

            return BadRequest();
        }
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> Get(int id)
    {
        try
        {
            return Ok(_Motherboard.Get(id));
        }
        catch (Exception)
        {

            return BadRequest();
        }

    }

    [HttpPost("")]
    public async Task<IActionResult> Post([FromBody] Motherboard entity)
    {
        try
        {
            _Motherboard.Add(entity);

            return Created("www.rtfmtool.com", entity);
        }
        catch (Exception)
        {

            return BadRequest();
        }

    }

    [HttpPut("")]
    public async Task<IActionResult> Put([FromBody] Motherboard entity)
    {
        try
        {
            _Motherboard.Update(entity);

            return Ok(entity);
        }
        catch (Exception)
        {

            return BadRequest();
        }
    }

    [HttpDelete("")]
    public async Task<IActionResult> Delete([FromBody] Motherboard entity)
    {
        try
        {
            _Motherboard.Remove(entity);
            return NoContent();
        }
        catch (Exception)
        {
            return BadRequest();
        }
    }
}
