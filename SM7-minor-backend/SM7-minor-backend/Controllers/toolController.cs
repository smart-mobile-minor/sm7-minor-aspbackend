﻿namespace SM7_minor_backend.Controllers;

using Microsoft.AspNetCore.Mvc;

[ApiController]
[Route("api/[controller]")]
public class toolController : Controller
{
    private readonly toolRepository _Tool;

    public toolController()
    {
        _Tool = new toolRepository(new rtfmDbContext());
    }

    [HttpGet("")]
    public async Task<IActionResult> GetAll()
    {
        try
        {
            return Ok(_Tool.GetAll());
        }
        catch (Exception)
        {

            return BadRequest();
        }
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> Get(int id)
    {
        try
        {
            return Ok(_Tool.Get(id));
        }
        catch (Exception)
        {

            return BadRequest();
        }

    }

    [HttpPost("")]
    public async Task<IActionResult> Post([FromBody] Tool entity)
    {
        try
        {
            _Tool.Add(entity);

            return Created("www.rtfmtool.com", entity);
        }
        catch (Exception)
        {

            return BadRequest();
        }

    }

    [HttpPut("")]
    public async Task<IActionResult> Put([FromBody] Tool entity)
    {
        try
        {
            _Tool.Update(entity);

            return Ok(entity);
        }
        catch (Exception)
        {

            return BadRequest();
        }
    }

    [HttpDelete("")]
    public async Task<IActionResult> Delete([FromBody] Tool entity)
    {
        try
        {
            _Tool.Remove(entity);
            return NoContent();
        }
        catch (Exception)
        {
            return BadRequest();
        }
    }
}
