﻿namespace SM7_minor_backend.Controllers;

using Microsoft.AspNetCore.Mvc;

[ApiController]
[Route("api/[controller]")]
public class powersupplyController : Controller
{
    private readonly powersupplyRepository _Powersupply;

    public powersupplyController()
    {
        _Powersupply = new powersupplyRepository(new rtfmDbContext());
    }

    [HttpGet("")]
    public async Task<IActionResult> GetAll()
    {
        try
        {
            return Ok(_Powersupply.GetAll());
        }
        catch (Exception)
        {

            return BadRequest();
        }
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> Get(int id)
    {
        try
        {
            return Ok(_Powersupply.Get(id));
        }
        catch (Exception)
        {

            return BadRequest();
        }

    }

    [HttpPost("")]
    public async Task<IActionResult> Post([FromBody] Powersupply entity)
    {
        try
        {
            _Powersupply.Add(entity);

            return Created("www.rtfmtool.com", entity);
        }
        catch (Exception)
        {

            return BadRequest();
        }

    }

    [HttpPut("")]
    public async Task<IActionResult> Put([FromBody] Powersupply entity)
    {
        try
        {
            _Powersupply.Update(entity);

            return Ok(entity);
        }
        catch (Exception)
        {

            return BadRequest();
        }
    }

    [HttpDelete("")]
    public async Task<IActionResult> Delete([FromBody] Powersupply entity)
    {
        try
        {
            _Powersupply.Remove(entity);
            return NoContent();
        }
        catch (Exception)
        {
            return BadRequest();
        }
    }
}
