﻿namespace SM7_minor_backend.Controllers;

using Microsoft.AspNetCore.Mvc;

[ApiController]
[Route("api/[controller]")]
public class processorController : Controller
{
    private readonly processorRepository _Processor;

    public processorController()
    {
        _Processor = new processorRepository(new rtfmDbContext());
    }
    
    [HttpGet("")]
    public async Task<IActionResult> GetAll()
    {
        try
        {
            return Ok(_Processor.GetAll());
        }
        catch (Exception)
        {

            return BadRequest();
        }
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> Get(int id)
    {
        try
        {
            return Ok(_Processor.Get(id));
        }
        catch (Exception)
        {

            return BadRequest();
        }
        
    }

    [HttpPost("")]
    public async Task<IActionResult> Post([FromBody] Processor entity)
    {
        try
        {
            _Processor.Add(entity);

            return Created("www.rtfmtool.com", entity);
        }
        catch (Exception)
        {

            return BadRequest();
        }
        
    }

    [HttpPut("")]
    public async Task<IActionResult> Put([FromBody] Processor entity)
    {
        try
        {
            _Processor.Update(entity);

            return Ok(entity);
        }
        catch (Exception)
        {

            return BadRequest();
        }
    }

    [HttpDelete("")]
    public async Task<IActionResult> Delete([FromBody] Processor entity)
    {
        try
        {
            _Processor.Remove(entity);
            return NoContent();
        }
        catch (Exception)
        {
            return BadRequest();
        }
    }
}



