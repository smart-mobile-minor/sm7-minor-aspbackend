﻿namespace SM7_minor_backend.Models;
public class AssemblyStep
{
    public int id { get; set; }
    public string name { get; set; }
    public string description { get; set; }
    public List<string> armodels { get; set; }
    public string sources { get; set; }
    public string video { get; set; }

    public AssemblyStep(int id, string name, string description, List<string> armodels, string sources, string video)
    {
        this.id = id;
        this.name = name;
        this.description = description;
        this.armodels = armodels;
        this.sources = sources;
        this.video = video;
    }
}