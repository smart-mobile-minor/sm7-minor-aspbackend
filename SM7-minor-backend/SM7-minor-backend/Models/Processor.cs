﻿namespace SM7_minor_backend.Models;

public class Processor
{
    public int id {  get; set; }
    public string name { get; set; }
    public ProcessorManufacturerEnum brand { get; set; }
    public string socketType { get; set; }
    public string image { get; set; }

    public Processor(int id, string name, ProcessorManufacturerEnum brand, string socketType, string image)
    {
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.socketType = socketType;
        this.image = image;
    }
}

