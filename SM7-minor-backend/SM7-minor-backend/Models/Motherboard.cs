﻿namespace SM7_minor_backend.Models;
public class Motherboard
{
    public int id {  get; set; }
    public string name { get; set; }
    public MotherboardManufacturerEnum brand { get; set; }
    public MotherboardSizeEnum size { get; set; }
    public string socketType { get; set; }
    public string image { get; set; }

    public Motherboard(int id, string name, MotherboardManufacturerEnum brand, MotherboardSizeEnum size ,string socketType, string image)
    {
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.size = size;
        this.socketType = socketType;
        this.image = image;
    }
}
