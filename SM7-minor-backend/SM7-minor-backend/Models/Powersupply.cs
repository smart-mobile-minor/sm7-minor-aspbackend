﻿namespace SM7_minor_backend.Models;
public class Powersupply
{
    public int id {  get; set; }
    public string name { get; set; }
    public PsuManufacturerEnum brand { get; set; }
    public PsuTypeEnum type { get; set; }
    public int capacity { get; set; }
    public string image { get; set; }

    public Powersupply(int id, string name, PsuManufacturerEnum brand, PsuTypeEnum type, int capacity, string image)
    {
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.type = type;
        this.capacity = capacity;
        this.image = image;
    }
}
