﻿namespace SM7_minor_backend.Models;
public class Build
{
    public Processor processor { get; set; }
    public Motherboard motherboard { get; set; }
    public Case pcCase { get; set; }
    public Powersupply powersupply { get; set; }

    public Build(Processor processor, Motherboard motherboard, Case pcCase, Powersupply powersupply)
    {
        this.processor = processor;
        this.motherboard = motherboard;
        this.pcCase = pcCase;
        this.powersupply = powersupply;
    }
}
