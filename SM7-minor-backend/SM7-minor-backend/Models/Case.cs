﻿namespace SM7_minor_backend.Models;

public class Case
{
    public int id { get; set; }
    public string name { get; set; }
    public CaseManufacturerEnum brand { get; set; }
    public MotherboardSizeEnum size { get; set; }
    public string image { get; set; }

    public Case(int id, string name, CaseManufacturerEnum brand, MotherboardSizeEnum size, string image)
    {
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.size = size;
        this.image = image;
    }
}

