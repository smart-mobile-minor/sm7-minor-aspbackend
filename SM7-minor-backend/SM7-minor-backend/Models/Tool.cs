﻿namespace SM7_minor_backend.Models;

public class Tool
{
    public int id { get; set; }
    public string name { get; set; }
    public string description { get; set; } 
    public string image { get; set; }
    public string video { get; set; }

    public Tool(int id, string name, string description, string image, string video)
    {
        this.id = id;
        this.name = name;
        this.description = description; 
        this.image = image;
        this.video = video;
    }
}

