﻿namespace SM7_minor_backend.Models;
public class ArModel
{
    public int id { get; set; }
    public string name { get; set; }

    public ArModel(int id, string name)
    {
        this.id = id;
        this.name = name;
    }
    
    
}